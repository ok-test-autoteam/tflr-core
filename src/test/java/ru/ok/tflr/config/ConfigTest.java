package ru.ok.tflr.config;

import org.junit.Test;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.reviewer.config.RuleEntry;
import ru.ok.tflr.reviewer.config.RulesConfigYaml;
import ru.ok.tflr.reviewer.config.RulesConfigYamlParser;
import ru.ok.tflr.reviewer.config.RulesLoader;
import ru.ok.tflr.tool.abstracts.TFLRRule;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ConfigTest {

    @Test
    public void testParseRules() {
        final String rulesConfigYamlFileName = "src/test/resources/main_test.yml";

        RulesConfigYaml rulesConfigYaml = RulesConfigYamlParser.parseRulesConfigYaml(rulesConfigYamlFileName);

        assertEquals("ru.ok.tflr.tool.primalcheckouts.PrimalCheckoutJP", rulesConfigYaml.getPrimalChecker());
        assertEquals(3, rulesConfigYaml.getRules().size());

        Iterator<RuleEntry> iterator = rulesConfigYaml.iterator();
        RuleEntry ruleEntry = iterator.next();
        assertEquals("ru.ok.tflr.rules.javaparser.TestAnnotationsJPRule", ruleEntry.getName());
        assertEquals(1, ruleEntry.getPriority());

        ruleEntry = iterator.next();
        assertEquals("ru.ok.tflr.rules.javaparser.CascadingIfStmtsJPRule", ruleEntry.getName());
        assertEquals(3, ruleEntry.getPriority());
    }

    @Test
    public void testLoadRules() {
        final String rulesConfigYamlFileName = "src/test/resources/main_test.yml";
        RulesConfigYaml rulesConfigYaml = RulesConfigYamlParser.parseRulesConfigYaml(rulesConfigYamlFileName);
        assertEquals(3, rulesConfigYaml.getRules().size());

        List<TFLRRule> allTflrRules = RulesLoader.loadRules(rulesConfigYaml, TFLRRulePriority.LOW);
        assertEquals(2, allTflrRules.size());

        allTflrRules = RulesLoader.loadRules(rulesConfigYaml, TFLRRulePriority.HIGH);
        assertEquals(1, allTflrRules.size());
    }
}
