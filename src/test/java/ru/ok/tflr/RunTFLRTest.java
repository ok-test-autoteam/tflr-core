package ru.ok.tflr;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.reviewer.TFLRStatus;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static ru.ok.tflr.TFLR.runTFLR;

public class RunTFLRTest {

    private static final Logger LOG = Logger.getLogger(RunTFLRTest.class.getName());

    @Test
    public void testRun() {
        final List<String> listSourceFiles = Collections.singletonList("src/test/java/ru/ok/tflr/RunTFLRTest.java");
        final String rulesConfigYaml = "src/test/resources/main_test.yml";
        final TFLRRulePriority minPriority = TFLRRulePriority.LOW;

        TFLRResult result = runTFLR(listSourceFiles, rulesConfigYaml, minPriority);
        assertEquals(TFLRStatus.VIOLATIONS_FOUND, result.getStatus());
        assertEquals(1, result.getReports().values().size());
        assertEquals(4, result.getReports().values().iterator().next().getViolations().size());
    }

    @Test
    public void testForTest() {
        if (true) {
            assertTrue(true);
        } else {
            assertTrue(true);
        }
        assertTrue(true);
    }

    @Test
    public void anotherTestForTest() {
        if (true) {
            assertTrue(true);
        } else if (true) {
            assertTrue(false);
        } else {
            assertTrue(true);
        }
        assertTrue(true);
    }
}
