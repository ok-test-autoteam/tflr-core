package ru.ok.tflr.rules;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import org.junit.Test;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.rules.javaparser.CascadingIfStmtsJPRule;
import ru.ok.tflr.rules.javaparser.TestAnnotationsJPRule;
import ru.ok.tflr.rules.javaparser.FindTestFilesJPRule;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static org.junit.Assert.*;

public class RulesTest {

    @Test
    public void testFindTestJPRules() throws Exception {
        TestAnnotationsJPRule annotationsJPRule = new TestAnnotationsJPRule();
        FindTestFilesJPRule testFilesJPRule = new FindTestFilesJPRule();
        String filePath = "src/test/java/ru/ok/tflr/RunTFLRTest.java";
        File file = new File(filePath);
        CompilationUnit compilationUnit = JavaParser.parse(new FileInputStream(file));

        List<TFLRRuleViolation> run = annotationsJPRule.run(compilationUnit, filePath);
        assertEquals(3, run.size());

        run = testFilesJPRule.run(compilationUnit, filePath);
        assertEquals(1, run.size());


        filePath = "src/test/java/ru/ok/tflr/config/EmptyTest.java";
        file = new File(filePath);
        compilationUnit = JavaParser.parse(new FileInputStream(file));
        run = annotationsJPRule.run(compilationUnit, filePath);
        assertTrue(!run.isEmpty());

        run = testFilesJPRule.run(compilationUnit, filePath);
        assertTrue(!run.isEmpty());
    }

    @Test
    public void testCascadingIfStmtsJPRule() throws Exception {
        CascadingIfStmtsJPRule cascadingIfStmtsJPRule = new CascadingIfStmtsJPRule();
        String filePath = "src/test/java/ru/ok/tflr/RunTFLRTest.java";
        File file = new File(filePath);
        CompilationUnit compilationUnit = JavaParser.parse(new FileInputStream(file));

        List<TFLRRuleViolation> run = cascadingIfStmtsJPRule.run(compilationUnit, filePath);
        assertEquals(1, run.size());
    }
}
