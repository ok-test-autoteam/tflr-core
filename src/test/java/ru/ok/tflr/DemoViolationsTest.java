package ru.ok.tflr;

import org.apache.log4j.Logger;
import org.junit.Test;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.reviewer.TFLRStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static ru.ok.tflr.TFLR.runTFLR;

public class DemoViolationsTest {

    private static final Logger LOG = Logger.getLogger(DemoViolationsTest.class.getName());
    private static final Logger L = Logger.getLogger(TFLR.class.getName());

    @Test
    public void testRun() {
        final List<String> listSourceFiles = new ArrayList<>();
        listSourceFiles.add("src/test");
        final String rulesConfigYaml = "src/main/resources/config_example.yml";
        final TFLRRulePriority minPriority = TFLRRulePriority.LOW;

        TFLRResult result = runTFLR(listSourceFiles, rulesConfigYaml, minPriority);
        assertEquals(TFLRStatus.VIOLATIONS_FOUND, result.getStatus());
    }

    /**
     * Good Javadoc.
     */
    @Test
    public void anotherTestForTest() {
        if (true) {
            assertTrue(true);
        } else if (true) {
            assertTrue(true);
        } else {
            assertTrue(true);
        }
        assertTrue(true);
        switch (1) {
            case 1:
                break;
            default:
                break;
        }
        do break; while (true);
        for (String s : new String[]{"", ""}) break;
        for (int i = 0; i < 5; i++) break;
        while (true) break;
    }
}
