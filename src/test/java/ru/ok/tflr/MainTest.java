package ru.ok.tflr;

import org.junit.Test;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.reviewer.TFLRStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static ru.ok.tflr.TFLR.runTFLR;

public class MainTest {

    @Test
    public void testMain() {
        final List<String> listSourceFiles = new ArrayList<>();
        listSourceFiles.add("src");
        final String rulesConfigYaml = "src/main/resources/config_example.yml";
        final TFLRRulePriority minPriority = TFLRRulePriority.LOW;

        TFLRResult result = runTFLR(listSourceFiles, rulesConfigYaml, minPriority);
        assertEquals(TFLRStatus.VIOLATIONS_FOUND, result.getStatus());
    }
}
