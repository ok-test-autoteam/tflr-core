package ru.ok.tflr.rules.pmd;

import ru.ok.tflr.tool.impl.pmd.TFLRAbstractPmdRule;

public class CommentDefaultAccessModifierRule extends TFLRAbstractPmdRule {

    public CommentDefaultAccessModifierRule() {
        super();
        this.setName(CommentDefaultAccessModifierRule.class.getSimpleName());
        this.setPmdRuleClassName("net.sourceforge.pmd.lang.java.rule.codestyle.CommentDefaultAccessModifierRule");
        this.setMessage("Missing commented default access modifier");
        this.setDescription("To avoid mistakes if we want that a Method, Constructor, Field or Nested class have a default access modifier\n" +
                "we must add a comment at the beginning of it's declaration.\n" +
                "By default the comment must be /* default */, if you want another, you have to provide a regexp.");
    }
}
