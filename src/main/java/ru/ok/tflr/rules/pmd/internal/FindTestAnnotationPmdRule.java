package ru.ok.tflr.rules.pmd.internal;

import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.rule.*;
import net.sourceforge.pmd.lang.java.ast.*;

import java.util.List;

public class FindTestAnnotationPmdRule extends AbstractJavaRule {

    @Override
    public Object visit(ASTCompilationUnit node, Object data) {

        List<ASTAnnotation> annotationList = node.findDescendantsOfType(ASTAnnotation.class);

        if (hasTestAnnotation(annotationList)) {
            addViolation(data, node);
        }

        return super.visit(node, data);
    }

    private boolean hasTestAnnotation(List<ASTAnnotation> annotationList) {
        for (ASTAnnotation annotation : annotationList) {
            Node annotationTypeNode = annotation.jjtGetChild(0);
            TypeNode annotationType = (TypeNode) annotationTypeNode;
            if (annotationType.getType() == null) {
                ASTName name = annotationTypeNode.getFirstChildOfType(ASTName.class);
                if (name != null && (name.hasImageEqualTo("Test"))) {
                    return true;
                }
            }
        }
        return false;
    }
}
