package ru.ok.tflr.rules.pmd;

import ru.ok.tflr.rules.pmd.internal.FindTestAnnotationPmdRule;
import ru.ok.tflr.tool.impl.pmd.TFLRAbstractPmdRule;

public class FindTestAnnotationRule extends TFLRAbstractPmdRule {

    public FindTestAnnotationRule() {
        super();
        this.setName(FindTestAnnotationRule.class.getSimpleName());
        this.setPmdRuleClassName(FindTestAnnotationPmdRule.class.getName());
        this.setMessage("Test class found (has method uses the @Test annotation)");
        this.setDescription("Class is for testing if there are methods annotated with the @Test annotation.");
    }
}
