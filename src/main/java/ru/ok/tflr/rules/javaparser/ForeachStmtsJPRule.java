package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.stmt.ForeachStmt;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class ForeachStmtsJPRule extends TFLRAbstractJPRule {

    public ForeachStmtsJPRule() {
        super();
        this.setName(ForeachStmtsJPRule.class.getSimpleName());
        this.setMessage("Foreach cycle - avoid cycles in test classes");
        this.setDescription("Foreach cycle - avoid cycles in test classes.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<ForeachStmt> foreachStmts = compilationUnit.findAll(ForeachStmt.class);
        for (ForeachStmt expr : foreachStmts) {
            TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
            tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
            tflrRuleViolations.add(tflrRuleViolation);
        }
        return tflrRuleViolations;
    }
}
