package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.stmt.DoStmt;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class DoStmtsJPRule extends TFLRAbstractJPRule {

    public DoStmtsJPRule() {
        super();
        this.setName(DoStmtsJPRule.class.getSimpleName());
        this.setMessage("Do-while cycle - avoid cycles in test classes");
        this.setDescription("Do-while cycle - avoid cycles in test classes.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<DoStmt> doStmts = compilationUnit.findAll(DoStmt.class);
        for (DoStmt expr : doStmts) {
            TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
            tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
            tflrRuleViolations.add(tflrRuleViolation);
        }
        return tflrRuleViolations;
    }
}
