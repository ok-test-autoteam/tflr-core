package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.stmt.SwitchStmt;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class SwitchStmtsJPRule extends TFLRAbstractJPRule {

    public SwitchStmtsJPRule() {
        super();
        this.setName(SwitchStmtsJPRule.class.getSimpleName());
        this.setMessage("Switch statement - avoid complex branching");
        this.setDescription("Switch statement - avoid complex branching.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<SwitchStmt> switchStmts = compilationUnit.findAll(SwitchStmt.class);
        for (SwitchStmt expr : switchStmts) {
            TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
            tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
            tflrRuleViolations.add(tflrRuleViolation);
        }
        return tflrRuleViolations;
    }
}
