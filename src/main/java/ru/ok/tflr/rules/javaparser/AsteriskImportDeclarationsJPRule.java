package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class AsteriskImportDeclarationsJPRule extends TFLRAbstractJPRule {

    public AsteriskImportDeclarationsJPRule() {
        super();
        this.setName(AsteriskImportDeclarationsJPRule.class.getSimpleName());
        this.setMessage("Import declaration with * - avoid importing all classes from the package");
        this.setDescription("Import declaration with * - avoid importing all classes from the package.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<ImportDeclaration> importDeclarations = compilationUnit.findAll(ImportDeclaration.class);
        for (ImportDeclaration expr : importDeclarations) {
            if (expr.isAsterisk()) {
                TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
                tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
                tflrRuleViolations.add(tflrRuleViolation);
            }
        }
        return tflrRuleViolations;
    }
}
