package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.stmt.ForStmt;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class ForStmtsJPRule extends TFLRAbstractJPRule {

    public ForStmtsJPRule() {
        super();
        this.setName(ForStmtsJPRule.class.getSimpleName());
        this.setMessage("For cycle - avoid cycles in test classes");
        this.setDescription("For cycle - avoid cycles in test classes.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<ForStmt> forStmts = compilationUnit.findAll(ForStmt.class);
        for (ForStmt expr : forStmts) {
            TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
            tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
            tflrRuleViolations.add(tflrRuleViolation);
        }
        return tflrRuleViolations;
    }
}
