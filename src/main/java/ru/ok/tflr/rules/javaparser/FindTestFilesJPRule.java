package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class FindTestFilesJPRule extends TFLRAbstractJPRule {

    public FindTestFilesJPRule() {
        super();
        this.setName(FindTestFilesJPRule.class.getSimpleName());
        this.setMessage("Test class found (has method uses the @Test annotation)");
        this.setDescription("Class is for testing if there are methods annotated with the @Test annotation.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<MarkerAnnotationExpr> markerAnnotationExprs = compilationUnit.findAll(MarkerAnnotationExpr.class);
        for (MarkerAnnotationExpr expr : markerAnnotationExprs) {
            if (expr.getNameAsString().equals("Test")) {
                TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
                tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
                tflrRuleViolations.add(tflrRuleViolation);
                return tflrRuleViolations;
            }
        }
        return tflrRuleViolations;
    }
}
