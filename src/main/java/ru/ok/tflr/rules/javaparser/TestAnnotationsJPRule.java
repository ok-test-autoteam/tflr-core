package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class TestAnnotationsJPRule extends TFLRAbstractJPRule {

    public TestAnnotationsJPRule() {
        super();
        this.setName(TestAnnotationsJPRule.class.getSimpleName());
        this.setMessage("Has method annotated with @Test");
        this.setDescription("Class is for testing if there are methods annotated with the @Test annotation.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<MarkerAnnotationExpr> markerAnnotationExprs = compilationUnit.findAll(MarkerAnnotationExpr.class);
        for (MarkerAnnotationExpr expr : markerAnnotationExprs) {
            if (expr.getNameAsString().equals("Test")) {
                TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
                tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
                tflrRuleViolations.add(tflrRuleViolation);
            }
        }
        return tflrRuleViolations;
    }
}
