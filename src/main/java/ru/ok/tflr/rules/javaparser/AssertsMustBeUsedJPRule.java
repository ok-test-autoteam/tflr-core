package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class AssertsMustBeUsedJPRule extends TFLRAbstractJPRule {

    public AssertsMustBeUsedJPRule() {
        super();
        this.setName(AssertsMustBeUsedJPRule.class.getSimpleName());
        this.setMessage("Asserts must be used in test classes");
        this.setDescription("Asserts must be used in test classes.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<MethodCallExpr> methodCallExprs = compilationUnit.findAll(MethodCallExpr.class);
        for (MethodCallExpr expr : methodCallExprs) {
            if (expr.getName().asString().contains("assert")) return tflrRuleViolations;
        }
        TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
        compilationUnit.findFirst(ClassOrInterfaceDeclaration.class).ifPresent((classDeclaration) ->
                tflrRuleViolation.setClassName(classDeclaration.getNameAsString()));
        compilationUnit.getPackageDeclaration().ifPresent((packageDeclaration) ->
                tflrRuleViolation.setPackageName(packageDeclaration.getNameAsString()));
        tflrRuleViolations.add(tflrRuleViolation);
        return tflrRuleViolations;
    }
}
