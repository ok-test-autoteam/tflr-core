package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class MustHaveJavadocJPRule extends TFLRAbstractJPRule {

    public MustHaveJavadocJPRule() {
        super();
        this.setName(MustHaveJavadocJPRule.class.getSimpleName());
        this.setMessage("Method or class without Javadoc - add Javadoc");
        this.setDescription("Method or class without Javadoc - add Javadoc.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<ClassOrInterfaceDeclaration> classOrInterfaceDeclarations = compilationUnit.findAll(ClassOrInterfaceDeclaration.class);
        for (ClassOrInterfaceDeclaration expr : classOrInterfaceDeclarations) {
            if (!expr.hasJavaDocComment()) {
                TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
                tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
                tflrRuleViolations.add(tflrRuleViolation);
            }
        }

        List<MethodDeclaration> methodDeclarations = compilationUnit.findAll(MethodDeclaration.class);
        for (MethodDeclaration expr : methodDeclarations) {
            if (!expr.hasJavaDocComment()) {
                TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
                tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
                tflrRuleViolations.add(tflrRuleViolation);
            }
        }
        return tflrRuleViolations;
    }
}
