package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.stmt.WhileStmt;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class WhileStmtsJPRule extends TFLRAbstractJPRule {

    public WhileStmtsJPRule() {
        super();
        this.setName(WhileStmtsJPRule.class.getSimpleName());
        this.setMessage("While cycle - avoid cycles in test classes");
        this.setDescription("While cycle - avoid cycles in test classes.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<WhileStmt> whileStmts = compilationUnit.findAll(WhileStmt.class);
        for (WhileStmt expr : whileStmts) {
            TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
            tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
            tflrRuleViolations.add(tflrRuleViolation);
        }
        return tflrRuleViolations;
    }
}
