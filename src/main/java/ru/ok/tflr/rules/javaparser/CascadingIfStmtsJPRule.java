package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.stmt.IfStmt;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;

public class CascadingIfStmtsJPRule extends TFLRAbstractJPRule {

    public CascadingIfStmtsJPRule() {
        super();
        this.setName(CascadingIfStmtsJPRule.class.getSimpleName());
        this.setMessage("Cascading if statement - avoid complex branching");
        this.setDescription("Cascading if statement - avoid complex branching.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        List<IfStmt> ifStmts = compilationUnit.findAll(IfStmt.class);
        for (IfStmt expr : ifStmts) {
            if (expr.hasCascadingIfStmt()) {
                TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
                tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, expr);
                tflrRuleViolations.add(tflrRuleViolation);
            }
        }
        return tflrRuleViolations;
    }
}
