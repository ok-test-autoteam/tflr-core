package ru.ok.tflr.rules.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.ClassExpr;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.impl.javaparser.TFLRAbstractJPRule;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LoggerSameClassJPRule extends TFLRAbstractJPRule {

    public LoggerSameClassJPRule() {
        super();
        this.setName(LoggerSameClassJPRule.class.getSimpleName());
        this.setMessage("Logger has another class in declaration - change it to the current class");
        this.setDescription("Logger has another class in declaration - change it to the current class.");
    }

    @Override
    public List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath) {
        List<TFLRRuleViolation> tflrRuleViolations = new ArrayList<>();

        String className;
        Optional<ClassOrInterfaceDeclaration> classOrInterfaceDeclarationOptional = compilationUnit.findFirst(ClassOrInterfaceDeclaration.class);
        if (classOrInterfaceDeclarationOptional.isPresent()) {
            className = classOrInterfaceDeclarationOptional.get().getNameAsString();
        } else {
            return tflrRuleViolations;
        }

        List<FieldDeclaration> fieldDeclarations = compilationUnit.findAll(FieldDeclaration.class);
        for (FieldDeclaration expr : fieldDeclarations) {
            NodeList<VariableDeclarator> variableDeclarators = expr.getVariables();
            for (VariableDeclarator variableDeclarator : variableDeclarators) {
                if (variableDeclarator.getType().asString().contains("Logger")) {
                    List<ClassExpr> classExprs = expr.findAll(ClassExpr.class);
                    for (ClassExpr classExpr : classExprs) {
                        if (!classExpr.getType().asString().equals(className)) {
                            TFLRRuleViolation tflrRuleViolation = new TFLRRuleViolation(this, getMessage(), filePath);
                            tflrRuleViolation = setViolationFields(tflrRuleViolation, compilationUnit, classExpr);
                            tflrRuleViolations.add(tflrRuleViolation);
                        }
                    }
                }
            }
        }
        return tflrRuleViolations;
    }
}
