package ru.ok.tflr.rules;

import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.tool.abstracts.TFLRRule;

public abstract class TFLRAbstractRule implements TFLRRule {

    private String name;    // rule name
    private String message; // message when a violation occurs
    private String description; // detailed description

    private TFLRRulePriority priority;

    public TFLRAbstractRule() {
    }

    @Override
    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    @Override
    public String getMessage() {
        return message;
    }

    protected void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getDescription() {
        return description;
    }

    protected void setDescription(String description) {
        this.description = description;
    }

    @Override
    public TFLRRulePriority getPriority() {
        return priority;
    }

    @Override
    public void setPriority(TFLRRulePriority priority) {
        this.priority = priority;
    }
}
