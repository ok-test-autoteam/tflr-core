package ru.ok.tflr.reviewer;

import ru.ok.tflr.tool.abstracts.TFLRRule;

/**
 * RuleViolation contains all information about one occurrence of a rule violation.
 */
public class TFLRRuleViolation {

    private final TFLRRule rule; //TODO должно заполнятся
    private final String message;
    private String filename;

    private int beginLine = 0;
    private int beginColumn = 0;

    private int endLine = 0;
    private int endColumn = 0;

    private String packageName = "";
    private String className = "";

    public TFLRRuleViolation(TFLRRule rule, String message, String filename, int beginLine, int beginColumn, int endLine, int endColumn, String packageName, String className) {
        this(rule, message, filename, beginLine, beginColumn, endLine, endColumn);
        this.packageName = packageName;
        this.className = className;
    }

    public TFLRRuleViolation(TFLRRule rule, String message, String filename, int beginLine, int beginColumn, int endLine, int endColumn) {
        this(rule, message, filename);
        this.beginLine = beginLine;
        this.beginColumn = beginColumn;
        this.endLine = endLine;
        this.endColumn = endColumn;
    }

    public TFLRRuleViolation(TFLRRule rule, String message, String filename) {
        this.rule = rule;
        this.message = message;
        this.filename = filename;
    }

    public void setViolationCodeNode(int beginLine, int beginColumn, int endLine, int endColumn) {
        this.beginLine = beginLine;
        this.beginColumn = beginColumn;
        this.endLine = endLine;
        this.endColumn = endColumn;
    }

    public TFLRRule getRule() {
        return rule;
    }

    public String getMessage() {
        return message;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getBeginLine() {
        return beginLine;
    }

    public void setBeginLine(int beginLine) {
        this.beginLine = beginLine;
    }

    public int getBeginColumn() {
        return beginColumn;
    }

    public void setBeginColumn(int beginColumn) {
        this.beginColumn = beginColumn;
    }

    public int getEndLine() {
        return endLine;
    }

    public void setEndLine(int endLine) {
        this.endLine = endLine;
    }

    public int getEndColumn() {
        return endColumn;
    }

    public void setEndColumn(int endColumn) {
        this.endColumn = endColumn;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
