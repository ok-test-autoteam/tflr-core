package ru.ok.tflr.reviewer;

import java.util.HashMap;
import java.util.Map;

/**
 * Result contains reports {@link TFLRReport} on all checked files which have rule violations.
 */
public class TFLRResult {

    // <filename of checked file, report of this file>
    private Map<String, TFLRReport> reports = new HashMap<>();
    private TFLRStatus status = TFLRStatus.NO_CHECKS_WERE_RUN;

    public TFLRResult() {
    }

    /**
     * Merges the given result into this result.
     * If there are different reports for the same file - merge this reports.
     *
     * @param result the result to be merged into this.
     */
    public TFLRResult merge(TFLRResult result) {
        //this merge needs tests
        result.getReports().keySet().forEach(
                filename -> this.reports.merge(filename, result.getReports().get(filename),
                        (r1, r2) -> {
                            r1.getViolations().addAll(r2.getViolations());
                            return r1;
                        }
                )
        );
        this.mergeStatus(result);
        return this;
    }

    private void mergeStatus(TFLRResult result) {
        if (this.status == TFLRStatus.ERROR || result.getStatus() == TFLRStatus.ERROR) {
            this.status = TFLRStatus.ERROR;
        } else if (this.status == TFLRStatus.WRONG_CONFIG || result.getStatus() == TFLRStatus.WRONG_CONFIG) {
            this.status = TFLRStatus.WRONG_CONFIG;
        } else if (this.status == TFLRStatus.VIOLATIONS_FOUND || result.getStatus() == TFLRStatus.VIOLATIONS_FOUND) {
            this.status = TFLRStatus.VIOLATIONS_FOUND;
        } else if (this.status == TFLRStatus.SUCCESS || result.getStatus() == TFLRStatus.SUCCESS) {
            this.status = TFLRStatus.SUCCESS;
        } else {
            this.status = TFLRStatus.NO_CHECKS_WERE_RUN;
        }
    }

    public Map<String, TFLRReport> getReports() {
        return reports;
    }

    public TFLRResult setReports(Map<String, TFLRReport> reports) {
        this.reports = reports;
        return this;
    }

    public TFLRStatus getStatus() {
        return status;
    }

    public TFLRResult setStatus(TFLRStatus status) {
        this.status = status;
        return this;
    }
}
