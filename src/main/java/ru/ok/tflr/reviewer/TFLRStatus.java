package ru.ok.tflr.reviewer;

public enum TFLRStatus {
    /**
     * SUCCESS: analysis completed successfully, no errors or violations found.
     */
    SUCCESS(0),
    /**
     * ERROR: there are some execution errors.
     */
    ERROR(1),
    /**
     * WRONG_CONFIG: there are some problems with config.
     */
    WRONG_CONFIG(2),
    /**
     * NO_CHECKS_WERE_RUN: initial status for TFLRResult.
     */
    NO_CHECKS_WERE_RUN(3),
    /**
     * VIOLATION_FOUND: analysis completed successfully, violations found.
     */
    VIOLATIONS_FOUND(4);

    private final int status;

    TFLRStatus(int status) {
        this.status = status;
    }

    public int getStatusId() {
        return status;
    }
}
