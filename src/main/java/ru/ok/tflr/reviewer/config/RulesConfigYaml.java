package ru.ok.tflr.reviewer.config;

import java.util.Iterator;
import java.util.List;

public class RulesConfigYaml implements Iterable<RuleEntry> {

    private String primalChecker;
    private List<RuleEntry> rules;

    public RulesConfigYaml() {
    }

    public RulesConfigYaml(List<RuleEntry> rules) {
        this.rules = rules;
    }

    public boolean isCorrect() {
        return getRules() != null && getPrimalChecker() != null;
    }

    @Override
    public Iterator<RuleEntry> iterator() {
        return rules.iterator();
    }

    public String getPrimalChecker() {
        return primalChecker;
    }

    public void setPrimalChecker(String primalChecker) {
        this.primalChecker = primalChecker;
    }

    public List<RuleEntry> getRules() {
        return rules;
    }

    public void setRules(List<RuleEntry> rules) {
        this.rules = rules;
    }
}
