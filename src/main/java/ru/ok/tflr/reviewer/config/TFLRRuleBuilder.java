package ru.ok.tflr.reviewer.config;

import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.tool.abstracts.TFLRRule;

import java.util.Optional;

public class TFLRRuleBuilder {

    private String className;
    private TFLRRulePriority priority;

    public TFLRRuleBuilder() {
    }

    public Optional<TFLRRule> build() throws ReflectiveOperationException {
        Object rule = TFLRRuleBuilder.class.getClassLoader().loadClass(className).getConstructor().newInstance();
        if (rule instanceof TFLRRule) {
            ((TFLRRule) rule).setPriority(priority);
            return Optional.of((TFLRRule) rule);
        }
        return Optional.empty();
    }

    public TFLRRuleBuilder setClassName(String className) {
        this.className = className;
        return this;
    }

    public TFLRRuleBuilder setPriority(TFLRRulePriority priority) {
        this.priority = priority;
        return this;
    }
}
