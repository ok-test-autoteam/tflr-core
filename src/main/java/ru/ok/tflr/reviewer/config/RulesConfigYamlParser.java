package ru.ok.tflr.reviewer.config;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class RulesConfigYamlParser {

    private static final Logger LOG = Logger.getLogger(RulesConfigYamlParser.class.getName());

    private RulesConfigYamlParser() {
    }

    public static RulesConfigYaml parseRulesConfigYaml(String rulesConfigYamlFileName) {
        Constructor constructor = new Constructor(RulesConfigYaml.class);
        TypeDescription configDescription = new TypeDescription(RulesConfigYaml.class);
        configDescription.addPropertyParameters("primalChecker", String.class);
        configDescription.addPropertyParameters("rules", RuleEntry.class);
        constructor.addTypeDescription(configDescription);
        Yaml yaml = new Yaml(constructor);

        try (InputStream inputStream = new FileInputStream(new File(rulesConfigYamlFileName))) {
            return yaml.load(inputStream);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            return new RulesConfigYaml();
        }
    }
}
