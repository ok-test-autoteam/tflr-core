package ru.ok.tflr.reviewer.config;

import org.apache.log4j.Logger;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.tool.abstracts.TFLRRule;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class RulesLoader {

    private static final Logger LOG = Logger.getLogger(RulesLoader.class.getName());
    private static final TFLRRuleBuilder TFLR_RULE_BUILDER = new TFLRRuleBuilder();

    private RulesLoader() {
    }

    public static List<TFLRRule> loadRules(RulesConfigYaml rulesConfigYaml, TFLRRulePriority minimumPriority) {
        List<TFLRRule> allTflrRules = new ArrayList<>();

        for (RuleEntry ruleEntry : rulesConfigYaml) {
            if (ruleEntry.getPriority() <= minimumPriority.getPriorityId()) {
                try {
                    Optional<TFLRRule> rule = TFLR_RULE_BUILDER.setClassName(ruleEntry.getName())
                            .setPriority(TFLRRulePriority.valueOf(ruleEntry.getPriority()))
                            .build();
                    rule.ifPresent(allTflrRules::add);
                } catch (ReflectiveOperationException e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        }
        return allTflrRules;
    }

    public static Object getCertainRules(List<TFLRRule> allTflrRules, Class<? extends TFLRRule> targetClass) {
        List<TFLRRule> certainRules = new ArrayList<>();

        for (TFLRRule rule : allTflrRules) {
            if (targetClass.isAssignableFrom(rule.getClass())) {
                certainRules.add(rule);
            }
        }
        return certainRules;
    }
}
