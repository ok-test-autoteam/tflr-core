package ru.ok.tflr.reviewer;

public enum TFLRRulePriority {
    /**
     * High (error): Change absolutely required.
     */
    HIGH(1),
    /**
     * Medium to high (error): Change highly recommended.
     */
    MEDIUM_HIGH(2),
    /**
     * Medium (warning): Change recommended.
     */
    MEDIUM(3),
    /**
     * Medium to low (warning): Change optional.
     */
    MEDIUM_LOW(4),
    /**
     * Low (warning): Change highly optional.
     */
    LOW(5);

    private final int priority;

    TFLRRulePriority(int priority) {
        this.priority = priority;
    }

    public int getPriorityId() {
        return priority;
    }

    /**
     * Get the priority which corresponds to the given number as returned by
     * {@link TFLRRulePriority#getPriorityId()}. If the number is an invalid value,
     * then {@link TFLRRulePriority#LOW} will be returned.
     *
     * @param priority The numeric priority value.
     * @return The priority.
     */
    public static TFLRRulePriority valueOf(int priority) {
        try {
            return TFLRRulePriority.values()[priority - 1];
        } catch (ArrayIndexOutOfBoundsException e) {
            return LOW;
        }
    }
}
