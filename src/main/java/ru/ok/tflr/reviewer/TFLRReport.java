package ru.ok.tflr.reviewer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Report contains all rule violations {@link TFLRRuleViolation} for one file.
 */
public class TFLRReport implements Iterable<TFLRRuleViolation> {

    private List<TFLRRuleViolation> violations = new ArrayList<>();

    public TFLRReport() {
    }

    public TFLRReport(List<TFLRRuleViolation> violations) {
        this.violations = violations;
    }

    @Override
    public Iterator<TFLRRuleViolation> iterator() {
        return violations.iterator();
    }

    public List<TFLRRuleViolation> getViolations() {
        return violations;
    }

    public void setViolations(List<TFLRRuleViolation> violations) {
        this.violations = violations;
    }
}
