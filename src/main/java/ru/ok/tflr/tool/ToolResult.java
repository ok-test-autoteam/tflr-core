package ru.ok.tflr.tool;

import ru.ok.tflr.reviewer.TFLRStatus;

import java.util.Map;

public class ToolResult<T> {

    private Map<String, T> reports;
    private TFLRStatus status;

    public ToolResult(Map<String, T> reports, TFLRStatus status) {
        this.reports = reports;
        this.status = status;
    }

    public Map<String, T> getReports() {
        return reports;
    }

    public ToolResult<T> setReports(Map<String, T> reports) {
        this.reports = reports;
        return this;
    }

    public TFLRStatus getStatus() {
        return status;
    }

    public ToolResult<T> setStatus(TFLRStatus status) {
        this.status = status;
        return this;
    }
}
