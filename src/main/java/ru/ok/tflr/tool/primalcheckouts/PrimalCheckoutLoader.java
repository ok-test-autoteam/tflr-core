package ru.ok.tflr.tool.primalcheckouts;

import org.apache.log4j.Logger;

import java.util.Optional;

public final class PrimalCheckoutLoader {

    private static final Logger LOG = Logger.getLogger(PrimalCheckoutLoader.class.getName());

    private PrimalCheckoutLoader() {
    }

    /**
     * Loads class by name and returns Optional with PrimalCheckout if loaded class is instance of PrimalCheckout.
     * If not - returns empty Optional.
     *
     * @param className class to load
     * @return Optional with PrimalCheckout or empty Optional
     */
    public static Optional<PrimalCheckout> loadChecker(String className) {
        try {
            Object checker = PrimalCheckoutLoader.class.getClassLoader().loadClass(className).getConstructor().newInstance();
            if (checker instanceof PrimalCheckout) return Optional.of((PrimalCheckout) checker);
        } catch (ReflectiveOperationException e) {
            LOG.error(e.getMessage(), e);
            return Optional.empty();
        }
        return Optional.empty();
    }
}
