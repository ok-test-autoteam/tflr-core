package ru.ok.tflr.tool.primalcheckouts;

import org.apache.log4j.Logger;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.rules.javaparser.FindTestFilesJPRule;
import ru.ok.tflr.tool.ToolReviewer;
import ru.ok.tflr.tool.impl.javaparser.JPFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PrimalCheckoutJP implements PrimalCheckout {

    private static final Logger LOG = Logger.getLogger(PrimalCheckoutJP.class.getName());

    /**
     * Primal Checkout.
     * JavaParser runs on initial source code paths and finds all test files.
     *
     * @param sourcePaths list of all source file paths
     * @return TFLRResult object with violations on all test files
     */
    @Override
    public TFLRResult runPrimalCheckout(List<String> sourcePaths) {
        TFLRResult resultToolJP = new TFLRResult();
        try {
            resultToolJP = new ToolReviewer<>(new JPFactory())
                    .check(sourcePaths, Collections.singletonList(new FindTestFilesJPRule()))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.error(e.getMessage(), e);
        }
        return resultToolJP;
    }
}
