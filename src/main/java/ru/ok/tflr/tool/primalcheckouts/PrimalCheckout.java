package ru.ok.tflr.tool.primalcheckouts;

import ru.ok.tflr.reviewer.TFLRResult;

import java.util.List;

public interface PrimalCheckout {

    /**
     * Primal Checkout.
     * Runs on initial source code paths and finds all test files.
     *
     * @param sourcePaths list of all source file paths
     * @return TFLRResult object with violations on all test files
     */
    TFLRResult runPrimalCheckout(List<String> sourcePaths);
}
