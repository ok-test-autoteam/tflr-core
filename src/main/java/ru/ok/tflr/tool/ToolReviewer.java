package ru.ok.tflr.tool;

import ru.ok.tflr.tool.abstracts.*;
import ru.ok.tflr.reviewer.TFLRResult;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Example: new ToolReviewer<>(new PMDFactory())
 *                              .check(Collections.emptyList(), Collections.emptyList())
 *                                  .get()
 *
 * @param <T> type of report, which is created by tool
 * @param <I> type of config for input sources
 * @param <R> type of config for rules for tool
 */
public class ToolReviewer<T, I, R> {
    private ToolConfigurator<I, R> configurator;
    private ConverterToTFLR<T> converter;
    private ToolRunner<T, I, R> runner;

    public ToolReviewer(ToolFactory<T, I, R> factory) {
        this.converter = factory.createConverter();
        this.configurator = factory.createConfigurator();
        this.runner = factory.createRunner();
    }

    public CompletableFuture<TFLRResult> check(List<String> sourcePaths, List<TFLRRule> rules) {
        return CompletableFuture.supplyAsync(() -> {
            ToolResult<T> toolResult = runner.execute(
                    configurator.configSourceInput(sourcePaths),
                    configurator.configRules(rules)
            );

            return new TFLRResult()
                    .setReports(converter.convertToTFLRResult(toolResult.getReports()))
                    .setStatus(toolResult.getStatus());
        });
    }
}
