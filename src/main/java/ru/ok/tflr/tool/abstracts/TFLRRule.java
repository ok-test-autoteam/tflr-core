package ru.ok.tflr.tool.abstracts;

import ru.ok.tflr.reviewer.TFLRRulePriority;

public interface TFLRRule {

    String getName();

    String getMessage();

    String getDescription();

    TFLRRulePriority getPriority();
    void setPriority(TFLRRulePriority priority);
}
