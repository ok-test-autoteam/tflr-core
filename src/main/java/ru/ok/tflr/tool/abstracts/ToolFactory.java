package ru.ok.tflr.tool.abstracts;

public interface ToolFactory<T, I, R> {
    ToolRunner<T, I, R> createRunner();
    ConverterToTFLR<T> createConverter();
    ToolConfigurator<I, R> createConfigurator();
}
