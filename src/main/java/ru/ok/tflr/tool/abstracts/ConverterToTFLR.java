package ru.ok.tflr.tool.abstracts;

import ru.ok.tflr.reviewer.TFLRReport;

import java.util.HashMap;
import java.util.Map;

public interface ConverterToTFLR<T> {

    TFLRReport convertToTFLRReport(T report);

    default Map<String, TFLRReport> convertToTFLRResult(Map<String, T> toolReports) {
        Map<String, TFLRReport> tflrReports = new HashMap<>();
        toolReports.keySet().forEach(
                file -> tflrReports.put(file, convertToTFLRReport(toolReports.get(file)))
        );
        return tflrReports;
    }
}
