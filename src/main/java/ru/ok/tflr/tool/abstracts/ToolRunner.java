package ru.ok.tflr.tool.abstracts;

import ru.ok.tflr.tool.ToolResult;

public interface ToolRunner<T, I, R> {
    ToolResult<T> execute(I inputConfig, R ruleConfig);
}
