package ru.ok.tflr.tool.abstracts;

import java.util.List;

public interface ToolConfigurator<I, R> {
    I configSourceInput(List<String> sourceInputPath);
    R configRules(List<TFLRRule> allTflrRules);
}
