package ru.ok.tflr.tool.impl.pmd;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Generates XML-ruleset for PMD with all selected PMD rules.
 */
public final class RulesetFileGenerator {

    private static final Logger LOG = Logger.getLogger(RulesetFileGenerator.class.getName());
    private static final String NAMESPACE = "http://pmd.sourceforge.net/ruleset/2.0.0";

    private RulesetFileGenerator() {
    }

    /**
     * Creates XML-ruleset file for PMD with all PMD rules.
     *
     * @param tflrPmdRules our PMD rules
     * @return file - XML with PMD ruleset
     */
    public static File createRulesetXmlFile(List<TFLRAbstractPmdRule> tflrPmdRules) {
        File file = null;
        try {
            file = File.createTempFile("Ruleset", ".xml");
            file.deleteOnExit();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }

        return generateRuleset(tflrPmdRules, file);
    }

    private static File generateRuleset(List<TFLRAbstractPmdRule> pmdRules, File file) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            Document doc = factory.newDocumentBuilder().newDocument();

            Element root = doc.createElementNS(NAMESPACE, "ruleset");
            root.setAttribute("name", "Auto generated ruleset");
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:schemaLocation", "http://pmd.sourceforge.net/ruleset/2.0.0 http://pmd.sourceforge.net/ruleset_2_0_0.xsd");
            doc.appendChild(root);

            Element description = doc.createElementNS(NAMESPACE, "description");
            description.setTextContent("Auto generated ruleset, contains all selected PMD rules.");
            root.appendChild(description);

            for (TFLRAbstractPmdRule pmdRule : pmdRules) {
                root.appendChild(generateRuleNode(doc, pmdRule));
            }

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(file));

        } catch (ParserConfigurationException | TransformerException e) {
            LOG.error(e.getMessage(), e);
        }
        return file;
    }

    private static Node generateRuleNode(Document doc, TFLRAbstractPmdRule pmdRule) {
        Element rule = doc.createElementNS(NAMESPACE, "rule");
        rule.setAttribute("name", pmdRule.getName());
        rule.setAttribute("language", pmdRule.getLanguage());
        rule.setAttribute("message", pmdRule.getMessage());
        rule.setAttribute("class", pmdRule.getPmdRuleClassName());

        Element description = doc.createElementNS(NAMESPACE, "description");
        description.setTextContent(pmdRule.getDescription());
        rule.appendChild(description);

        Element priority = doc.createElementNS(NAMESPACE, "priority");
        priority.setTextContent(String.valueOf(pmdRule.getPriority().getPriorityId()));
        rule.appendChild(priority);

        return rule;
    }
}
