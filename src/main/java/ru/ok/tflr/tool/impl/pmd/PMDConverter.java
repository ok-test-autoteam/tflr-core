package ru.ok.tflr.tool.impl.pmd;

import net.sourceforge.pmd.Report;
import net.sourceforge.pmd.RuleViolation;
import ru.ok.tflr.reviewer.TFLRReport;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.tool.abstracts.ConverterToTFLR;

import java.util.ArrayList;
import java.util.List;

public class PMDConverter implements ConverterToTFLR<Report> {

    public TFLRReport convertToTFLRReport(Report report) {
        List<TFLRRuleViolation> violations = new ArrayList<>();

        for (RuleViolation pmdViolation : report) {
            violations.add(convertToTFLRRuleViolation(pmdViolation));
        }

        return new TFLRReport(violations);
    }

    private TFLRRuleViolation convertToTFLRRuleViolation(RuleViolation javaRuleViolation) {
        return new TFLRRuleViolation(null,
                javaRuleViolation.getDescription(),
                javaRuleViolation.getFilename(),
                javaRuleViolation.getBeginLine(),
                javaRuleViolation.getBeginColumn(),
                javaRuleViolation.getEndLine(),
                javaRuleViolation.getEndColumn(),
                javaRuleViolation.getPackageName(),
                javaRuleViolation.getClassName());
    }
}
