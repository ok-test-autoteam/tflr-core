package ru.ok.tflr.tool.impl.pmd;

import ru.ok.tflr.rules.TFLRAbstractRule;

public abstract class TFLRAbstractPmdRule extends TFLRAbstractRule {

    private String language = "java";
    private String pmdRuleClassName;

    public TFLRAbstractPmdRule() {
        super();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPmdRuleClassName() {
        return pmdRuleClassName;
    }

    public void setPmdRuleClassName(String pmdRuleClassName) {
        this.pmdRuleClassName = pmdRuleClassName;
    }
}
