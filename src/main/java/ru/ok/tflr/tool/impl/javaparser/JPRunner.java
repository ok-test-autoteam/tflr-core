package ru.ok.tflr.tool.impl.javaparser;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import org.apache.log4j.Logger;
import ru.ok.tflr.reviewer.TFLRReport;
import ru.ok.tflr.reviewer.TFLRStatus;
import ru.ok.tflr.tool.ToolResult;
import ru.ok.tflr.tool.abstracts.ToolRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TFLRReport - report, which is created by tool
 * List<File> - list of all source files (.java)
 * List<TFLRAbstractJPRule> - list of all JP rules, which will be ran on each source file
 */
public class JPRunner implements ToolRunner<TFLRReport, List<File>, List<TFLRAbstractJPRule>> {

    private final Logger LOG = Logger.getLogger(JPRunner.class.getName());

    @Override
    public ToolResult<TFLRReport> execute(List<File> files, List<TFLRAbstractJPRule> ruleConfig) {
        Map<String, TFLRReport> jpReports = new HashMap<>();
        TFLRStatus status = TFLRStatus.SUCCESS;
        if (ruleConfig.isEmpty()) return new ToolResult<>(jpReports, status);
        for (File file : files) {
            TFLRReport jpReport = new TFLRReport();
            try {
                CompilationUnit compilationUnit = JavaParser.parse(new FileInputStream(file));
                for (TFLRAbstractJPRule rule : ruleConfig) {
                    jpReport.getViolations().addAll(rule.run(compilationUnit, file.getAbsolutePath()));
                }
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            }
            if (!jpReport.getViolations().isEmpty()) {
                jpReports.put(file.getAbsolutePath(), jpReport);
                status = TFLRStatus.VIOLATIONS_FOUND;
            }
        }
        return new ToolResult<>(jpReports, status);
    }
}
