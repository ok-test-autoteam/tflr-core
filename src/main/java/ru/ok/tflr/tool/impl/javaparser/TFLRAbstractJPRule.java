package ru.ok.tflr.tool.impl.javaparser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import ru.ok.tflr.reviewer.TFLRRuleViolation;
import ru.ok.tflr.rules.TFLRAbstractRule;

import java.util.List;

public abstract class TFLRAbstractJPRule extends TFLRAbstractRule {

    public abstract List<TFLRRuleViolation> run(CompilationUnit compilationUnit, String filePath);

    /**
     * Fills RuleViolation's class name, package name and code node position.
     *
     * @param tflrRuleViolation RuleViolation to fill
     * @param compilationUnit   compilation unit need for class name and package name
     * @param expression        expression need for code node position
     * @return rule violation with filled class name, package name and code node position
     */
    protected TFLRRuleViolation setViolationFields(TFLRRuleViolation tflrRuleViolation, CompilationUnit compilationUnit, Node expression) {
        compilationUnit.findFirst(ClassOrInterfaceDeclaration.class).ifPresent((classDeclaration) ->
                tflrRuleViolation.setClassName(classDeclaration.getNameAsString()));
        compilationUnit.getPackageDeclaration().ifPresent((packageDeclaration) ->
                tflrRuleViolation.setPackageName(packageDeclaration.getNameAsString()));
        if (expression.getBegin().isPresent() && expression.getEnd().isPresent())
            tflrRuleViolation.setViolationCodeNode(expression.getBegin().get().line, expression.getBegin().get().column, expression.getEnd().get().line, expression.getEnd().get().column);
        return tflrRuleViolation;
    }
}
