package ru.ok.tflr.tool.impl.javaparser;

import ru.ok.tflr.reviewer.TFLRReport;
import ru.ok.tflr.tool.abstracts.ConverterToTFLR;

/**
 * TFLRReport - report, which is created by tool
 */
public class JPConverter implements ConverterToTFLR<TFLRReport> {

    @Override
    public TFLRReport convertToTFLRReport(TFLRReport report) {
        return report;
    }
}
