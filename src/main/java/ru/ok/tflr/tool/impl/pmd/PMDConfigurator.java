package ru.ok.tflr.tool.impl.pmd;

import ru.ok.tflr.reviewer.config.RulesLoader;
import ru.ok.tflr.tool.abstracts.TFLRRule;
import ru.ok.tflr.tool.abstracts.ToolConfigurator;

import java.util.List;

public class PMDConfigurator implements ToolConfigurator<String, String> {

    private static final String PMD_DELIMITER = ",";

    /**
     * Returns string with comma-divided file paths.
     */
    @Override
    public String configSourceInput(List<String> sourceInputPath) {
        return String.join(PMD_DELIMITER, sourceInputPath);
    }

    /**
     * Returns path to XML PMD-ruleset file.
     */
    @Override
    public String configRules(List<TFLRRule> allTflrRules) {
        if (getPmdRules(allTflrRules).isEmpty()) return null;
        return RulesetFileGenerator.createRulesetXmlFile(getPmdRules(allTflrRules)).getPath();
    }

    /**
     * Get only our PMD rules from all our rules.
     *
     * @param allTflrRules all our rules
     * @return our PMD rules
     */
    private List<TFLRAbstractPmdRule> getPmdRules(List<TFLRRule> allTflrRules) {
        return (List<TFLRAbstractPmdRule>) RulesLoader.getCertainRules(allTflrRules, TFLRAbstractPmdRule.class);
    }
}
