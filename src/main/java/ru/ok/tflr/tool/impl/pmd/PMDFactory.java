package ru.ok.tflr.tool.impl.pmd;

import net.sourceforge.pmd.Report;
import ru.ok.tflr.tool.abstracts.ConverterToTFLR;
import ru.ok.tflr.tool.abstracts.ToolConfigurator;
import ru.ok.tflr.tool.abstracts.ToolFactory;
import ru.ok.tflr.tool.abstracts.ToolRunner;

public class PMDFactory implements ToolFactory<Report, String, String> {

    @Override
    public ToolRunner<Report, String, String> createRunner() {
        return new PMDRunner();
    }

    @Override
    public ConverterToTFLR<Report> createConverter() {
        return new PMDConverter();
    }

    @Override
    public ToolConfigurator<String, String> createConfigurator() {
        return new PMDConfigurator();
    }
}
