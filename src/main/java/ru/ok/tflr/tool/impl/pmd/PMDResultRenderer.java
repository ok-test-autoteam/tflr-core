package ru.ok.tflr.tool.impl.pmd;

import net.sourceforge.pmd.Report;
import net.sourceforge.pmd.renderers.AbstractRenderer;
import net.sourceforge.pmd.util.datasource.DataSource;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PMDResultRenderer extends AbstractRenderer {

    // This HashMap contains all reports after ending of PMD checkout.
    private static final Map<String, Report> reports = new ConcurrentHashMap<>();

    public PMDResultRenderer() {
        super("pojoresult", "Java object result renderer");
    }

    @Override
    public String defaultFileExtension() {
        return "";
    }

    @Override
    public void start() {
        reports.clear();
    }

    @Override
    public void startFileAnalysis(DataSource dataSource) {
    }

    @Override
    public void renderFileReport(Report report) {
        if (!report.treeIsEmpty() || report.hasErrors() || report.hasConfigErrors())
            reports.put(report.iterator().next().getFilename(), report);
    }

    @Override
    public void end() {
    }

    public static Map<String, Report> getReports() {
        return reports;
    }
}