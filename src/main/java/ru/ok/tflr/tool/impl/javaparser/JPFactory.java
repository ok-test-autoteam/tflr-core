package ru.ok.tflr.tool.impl.javaparser;

import ru.ok.tflr.reviewer.TFLRReport;
import ru.ok.tflr.tool.abstracts.ConverterToTFLR;
import ru.ok.tflr.tool.abstracts.ToolConfigurator;
import ru.ok.tflr.tool.abstracts.ToolFactory;
import ru.ok.tflr.tool.abstracts.ToolRunner;

import java.io.File;
import java.util.List;

/**
 * TFLRReport - report, which is created by tool
 * List<File> - list of all files (.java)
 * List<TFLRAbstractJPRule> - list of all JP rules, which will be ran on each source file
 */
public class JPFactory implements ToolFactory<TFLRReport, List<File>, List<TFLRAbstractJPRule>> {

    @Override
    public ToolRunner<TFLRReport, List<File>, List<TFLRAbstractJPRule>> createRunner() {
        return new JPRunner();
    }

    @Override
    public ConverterToTFLR<TFLRReport> createConverter() {
        return new JPConverter();
    }

    @Override
    public ToolConfigurator<List<File>, List<TFLRAbstractJPRule>> createConfigurator() {
        return new JPConfigurator();
    }
}
