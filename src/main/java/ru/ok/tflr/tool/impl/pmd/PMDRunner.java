package ru.ok.tflr.tool.impl.pmd;

import net.sourceforge.pmd.PMD;
import net.sourceforge.pmd.PMDConfiguration;
import net.sourceforge.pmd.Report;
import net.sourceforge.pmd.RulePriority;
import org.apache.log4j.Logger;
import ru.ok.tflr.reviewer.TFLRStatus;
import ru.ok.tflr.tool.abstracts.ToolRunner;
import ru.ok.tflr.tool.ToolResult;

import java.util.Collections;
import java.util.Map;

public class PMDRunner implements ToolRunner<Report, String, String> {

    private final Logger LOG = Logger.getLogger(PMDRunner.class.getName());
    private final String REPORT_FORMAT = "ru.ok.tflr.tool.impl.pmd.PMDResultRenderer";

    /**
     * Performs PMD analysis.
     */
    @Override
    public ToolResult<Report> execute(String sourcePaths, String ruleset) {
        if (ruleset == null) return new ToolResult<>(Collections.emptyMap(), TFLRStatus.SUCCESS);

        TFLRStatus status = runPMD(createPmdConfig(sourcePaths, ruleset));
        Map<String, Report> pmdReports = PMDResultRenderer.getReports();

        return new ToolResult<>(pmdReports, status);
    }

    /**
     * This method is the main entry point to PMD.
     *
     * @param configuration the configure to use
     * @return status {@link TFLRStatus}
     */
    private TFLRStatus runPMD(PMDConfiguration configuration) {
        TFLRStatus status;

        try {
            Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
            int violations = PMD.doPMD(configuration);
            if (violations > 0 && configuration.isFailOnViolation()) {
                status = TFLRStatus.VIOLATIONS_FOUND;
            } else {
                status = TFLRStatus.SUCCESS;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            status = TFLRStatus.ERROR;
        }
        return status;
    }

    /**
     * Creates PMD Configuration for running PMD.
     *
     * @param sourcePaths string with all test file paths, comma-divided
     * @param ruleset     path to XML PMD-ruleset file
     * @return PMD config
     */
    private PMDConfiguration createPmdConfig(String sourcePaths, String ruleset) {
        PMDConfiguration configuration = new PMDConfiguration();

        configuration.setInputPaths(sourcePaths);
        configuration.setRuleSets(ruleset);
        configuration.setReportFormat(REPORT_FORMAT);
        configuration.setMinimumPriority(RulePriority.LOW);
        configuration.setIgnoreIncrementalAnalysis(true);
        configuration.setThreads(0);

        return configuration;
    }
}
