package ru.ok.tflr.tool.impl.javaparser;

import org.apache.log4j.Logger;
import ru.ok.tflr.reviewer.config.RulesLoader;
import ru.ok.tflr.tool.abstracts.TFLRRule;
import ru.ok.tflr.tool.abstracts.ToolConfigurator;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * List<File> - list of all source files (.java)
 * List<TFLRAbstractJPRule> - list of all JP rules, which will be ran on each source file
 */
public class JPConfigurator implements ToolConfigurator<List<File>, List<TFLRAbstractJPRule>> {

    private static final Logger LOG = Logger.getLogger(JPConfigurator.class.getName());

    /**
     * Finds all .java files. Performs file tree walking to find .java files in directories.
     *
     * @param sourceInputPaths list of files and directories with source files
     * @return list of source file (.java) paths
     */
    @Override
    public List<File> configSourceInput(List<String> sourceInputPaths) {
        List<File> allJavaFiles = new ArrayList<>();
        for (String sourceInputPath : sourceInputPaths) {
            File source = new File(sourceInputPath);
            if (source.exists() && source.isFile()) {
                if (source.getName().endsWith(".java")) allJavaFiles.add(source);
            } else if (source.exists() && source.isDirectory()) {
                FindJavaFileVisitor findJavaFileVisitor = new FindJavaFileVisitor();
                try {
                    Files.walkFileTree(source.toPath(), findJavaFileVisitor);
                } catch (IOException e) {
                    LOG.error(e.getMessage(), e);
                }
                allJavaFiles.addAll(findJavaFileVisitor.getJavaFiles());
            }
        }
        return allJavaFiles;
    }

    @Override
    public List<TFLRAbstractJPRule> configRules(List<TFLRRule> allTflrRules) {
        return (List<TFLRAbstractJPRule>) RulesLoader.getCertainRules(allTflrRules, TFLRAbstractJPRule.class);
    }

    /**
     * Finds .java files in directories.
     */
    private class FindJavaFileVisitor extends SimpleFileVisitor<Path> {

        private List<File> javaFiles;

        FindJavaFileVisitor() {
            javaFiles = new ArrayList<>();
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            File source = file.toFile();
            if (source.exists() && source.getName().endsWith(".java"))
                javaFiles.add(source);
            return FileVisitResult.CONTINUE;
        }

        List<File> getJavaFiles() {
            return javaFiles;
        }
    }
}
