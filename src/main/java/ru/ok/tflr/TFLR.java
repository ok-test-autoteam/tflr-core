package ru.ok.tflr;

import org.apache.log4j.Logger;
import ru.ok.tflr.reviewer.TFLRStatus;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.tool.ToolReviewer;
import ru.ok.tflr.tool.abstracts.TFLRRule;
import ru.ok.tflr.tool.abstracts.ToolFactory;
import ru.ok.tflr.tool.impl.javaparser.JPFactory;
import ru.ok.tflr.tool.impl.pmd.*;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.reviewer.config.RulesConfigYaml;
import ru.ok.tflr.reviewer.config.RulesConfigYamlParser;
import ru.ok.tflr.reviewer.config.RulesLoader;
import ru.ok.tflr.tool.primalcheckouts.PrimalCheckout;
import ru.ok.tflr.tool.primalcheckouts.PrimalCheckoutLoader;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public final class TFLR {

    private static final Logger LOG = Logger.getLogger(TFLR.class.getName());

    private TFLR() {
    }

    /**
     * Main entry to TFLR analysis. Runs different tools on test files and returns one overall result object.
     *
     * @param listSourceFiles         list of all source file paths
     * @param rulesConfigYamlFileName path to RulesConfig.yml
     * @param minimumPriority         minimum rule priority to check
     * @return all rule violations formed in TFLRResult object
     */
    public static TFLRResult runTFLR(List<String> listSourceFiles, String rulesConfigYamlFileName, TFLRRulePriority minimumPriority) {
        RulesConfigYaml rulesConfigYaml;
        List<TFLRRule> allTflrRules;
        TFLRResult primalResult = new TFLRResult();

        // Config hasn't rules or primal checker
        rulesConfigYaml = RulesConfigYamlParser.parseRulesConfigYaml(rulesConfigYamlFileName);
        if (!rulesConfigYaml.isCorrect()) return primalResult.setStatus(TFLRStatus.WRONG_CONFIG);

        // Config has wrong primal checkout class
        Optional<PrimalCheckout> primalCheckout = PrimalCheckoutLoader.loadChecker(rulesConfigYaml.getPrimalChecker());
        if (!primalCheckout.isPresent()) return primalResult.setStatus(TFLRStatus.WRONG_CONFIG);

        // Config has wrong (not TFLRAbstractRule) classes
        allTflrRules = RulesLoader.loadRules(rulesConfigYaml, minimumPriority);
        if (allTflrRules.isEmpty()) return primalResult.setStatus(TFLRStatus.WRONG_CONFIG);

        //// Primal checkout
        primalResult.merge(primalCheckout.get().runPrimalCheckout(listSourceFiles));
        if (primalResult.getStatus() != TFLRStatus.VIOLATIONS_FOUND) {
            if (primalResult.getStatus() == TFLRStatus.SUCCESS) primalResult.setStatus(TFLRStatus.NO_CHECKS_WERE_RUN);
            return new TFLRResult().setStatus(primalResult.getStatus());
        }

        LOG.debug("Found test files:");
        primalResult.getReports().keySet().forEach(LOG::debug);

        //// Secondary checkout
        TFLRResult result = new TFLRResult();

        // Launch all tools
        List<ToolFactory> factories = Arrays.asList(new JPFactory(), new PMDFactory());
        List<CompletableFuture<TFLRResult>> futureResults = new ArrayList<>();
        factories.forEach((factory) -> futureResults.add((new ToolReviewer<>(factory)).check(new ArrayList<>(primalResult.getReports().keySet()), allTflrRules)));
        futureResults.forEach((futureResult) -> {
            try {
                result.merge(futureResult.get());
            } catch (ExecutionException | InterruptedException e) {
                LOG.error(e.getMessage(), e);
                result.setStatus(TFLRStatus.ERROR);
            }
        });
        return result;
    }
}