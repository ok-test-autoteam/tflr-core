Core
=============================

The main part of the TFLR project, which performs a static analysis of the test files of the project and generates the report in a certain format.

## How to build
1. Load project from repository and launch in IDEA;
2. Choose "Import gradle settings" and select "Use gradle 'wrapper' task configuration";
3. To create executive jar run gradle task "shadowJar" (View -> Tool windows -> Gradle --> tflr-core -> Tasks -> shadow -> shadowJar);
4. Executive jar located in tflr-core\build\libs.

## How to launch
1. Start tflr-core-1.0.0-all.jar by "java -jar tflr-core-1.0.0-all.jar";
2. Config.xml will be created in current directory, set input paths to source files and paths to rulesets;
3. Run "java -jar tflr-core-1.0.0-all.jar" again and search for results in secondaryOutput.xml.

### Command line params
"-dir", "-d" - Root directory for sources. You can set path to source files from command window, another settings will be obtained from the config.xml.